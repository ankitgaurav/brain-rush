# **Brain Rush**
A simple, yet addictive brain teasing game.
[Play Now][game url]

Made with love just for *fun* !

# Contributions
Changes and improvements are more than welcome!

Feel free to fork and open a pull request.
Please make your changes in a specific branch and request to pull into master!
If you can, please make sure the game fully works before sending the PR, as that will help speed up the process.

# License
Brian Rush ( repository name "brain-rush" ) is licensed under the **MIT license**.
[game url]:https://ankitgaurav.github.io/brain-rush
